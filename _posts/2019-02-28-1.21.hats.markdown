---
layout: post
title:  "1.21 Hats"
date:   2019-02-28
categories: release
---

- high resolution patterns
- hat templates
    - caps and toques (flexible template!)
    - new `ABOVE_HAIR` layer
- fixes
    - fix removing clothing mods even though clothes didn't exist

## How to hide hair parts
For some hairstyles you'll want to hide parts of it when wearing a hat.
You can do this by filling out the `coverConceal` property such as below:

{% highlight javascript %}
export class AsianBunBack extends HairPart {

    constructor(...data) {
        super(Hair.hairBack, {
            coverConceal: ["head"], // hide this part when clothing is worn on head
            }, ...data);
    }
    
    // other stuff
}
{% endhighlight %}

You can explicitly not cover parts that have `converConceal` by setting the `noCover` property to true in the
clothing template (for example if you're making a hair clip).

{% highlight javascript %}
// a Clothing class rather than a ClothingPart class
export class HairClip extends Accessory {
    constructor(...data) {
        super({noCover:true}, ...data);
    }

    // other stuff
}
{% endhighlight %}

## Cap template

With the cap template, you can either make caps or toques depending on parameter choice.

![toque](https://i.imgur.com/TvmOriD.png)
![cap](https://i.imgur.com/7mhPo2A.png)

{% highlight javascript %}
{
    // how high the top of the cap should be
    height      : 4.5,
    // [-0.5, 0.5] which side the cap is tilted towards (negative is top tilted towards left)
    sideBias    : -0.15,
    // degree of curve for bottom of hat
    curvature   : 7,
    // [-1, 1] (reasonable values) how much of the forehead should be covered (negative doesn't cover forehead)
    headCoverage: 0.2,
    // how wide to make the hat to adjust for ears and stuff
    sideOffset  : 2.3,
},
{% endhighlight %}

## Fixing low resolution patterns

There were some problems with patterns:
- vertically flipped
- low resolution

The first problem was easy to solve. The second problem was because fill patterns naturally scaled
with the drawing context. This meant for a scaling of 3, any pattern was also scaled up, losing
a lot of resolution. The resolution was to refactor the Context2DTracked wrapper to scale the input
points rather than the canvas.

![highres](https://i.imgur.com/YCxvSo3.png)