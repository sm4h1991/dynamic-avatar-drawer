---
layout: post
title:  "1.26 Asset Addition (Dahakma)"
date:   2019-10-07
categories: release
---

- new clothing from Dahakma: 
    - underwear
        - bikinis
        - panties with bow
        - bra with strap
    - makeup
        - lipstick
        - nails
    - neck accessories
        - crossed chokers
        - tagged collars
        - necklaces
    - jewelry
        - armlets
        - body chains
        - belly button piercings
        - earrings
    - tops
        - tube top with sleeves
        - swimsuit
        - leotard
    - bottoms
        - loincloth
        - jeans
    - hats
        - magic cap
        - maid headpiece
    - armor
        - cuirass
        - vambraces
        - greaves
- modified clothing calculations (using this)
    - calcSuperPants
    - calcSuperSocks
    - tube tops

Another big thanks to Dahakma for their work on creating so many assets!
All of these are templates with their own unique parameters, play around with them in the playground.

Sample outfits you can make with this version:

![outfit 1](https://i.imgur.com/zLVn9xZ.png)
![outfit 2](https://i.imgur.com/2bmFmCX.png)
![outfit 3](https://i.imgur.com/YWxw798.png)
![outfit 4](https://i.imgur.com/frk1ELj.png)

## Underwear
- SuperBra
- SuperPanties
- BikiniTop
- BikiniBottom

Sample of configurations (in addition to the usual fill and stroke options)
![super underwear 1](https://i.imgur.com/J2jshsd.png)
![super underwear 2](https://i.imgur.com/2wP3bBU.png)
![bikini 1](https://i.imgur.com/mU0GcZh.png)
![bikini 2](https://i.imgur.com/nNCv9l4.png)

## Tops
- TubeTopSleeves
- Leotard
- Swimsuit


## Accessories
- SimpleBelt
- Apron
