import { none } from "drawpoint/dist-esm";
import { BasePart } from "../parts/part";
import { Player } from "../player/player";
import { Layer } from "../util/canvas";

export const shine = "hsla(0,100%,100%,0.2)";
export const shadingDark = "hsl(0,15%,80%)";
export const shadingMedium = "hsl(0,15%,85%)";
export const shadingLight = "hsl(0,15%,90%)";

export class ShadingPart extends BasePart {
  constructor(...data) {
    super(
      Object.assign(
        {
          layer: Layer.FRONT
        },
        ...data
      )
    );
  }

  stroke(ctx?: CanvasRenderingContext2D, ex?: any) {
    return none;
  }

  fill(ctx?: CanvasRenderingContext2D, ex?: any) {
    return shadingDark;
  }

  getLineWidth(player?: Player) {
    return 0;
  }
}
